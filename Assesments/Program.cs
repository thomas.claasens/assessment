﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesments
{
    class Program
    {
        static void Main(string[] args)
        {
            var lineItems = ReadFile("data.csv");
            SortNamesbyCount(lineItems);

            SortListByAddress(lineItems);
          
        }

        private static void SortListByAddress(List<LineItem> lineItems)
        {
            if (lineItems == null)
                throw new ArgumentNullException(nameof(lineItems));
            //Sort by the street name part not the number.
            var orderAddress = lineItems
                .OrderBy(ord => ord.Address.Split(' ')[1]);

            TextWriter textWriter = new StreamWriter("ListByAddress.txt");
            orderAddress.All(a =>
            {
                textWriter.WriteLine(a.Address);
                return true;
            });
           
            textWriter.Close();
        }

        private static void SortNamesbyCount(List<LineItem> lineItems)
        {
            if (lineItems == null)
                throw new ArgumentNullException(nameof(lineItems));
            var namesList = new List<string>();
            foreach (var lineItem in lineItems)
            {
                namesList.Add(lineItem.FirstName);
                namesList.Add(lineItem.LastName);

            }

            var orderedItems =
                namesList
                    .GroupBy(li => li)
                    .OrderByDescending(g => g.Count())
                    .ThenBy(g => g.Key)
                    .ToDictionary(g => g.Key, g => g.Count());

            TextWriter textWriter = new StreamWriter("NamesbyCount.txt");
            orderedItems.All(a =>
            {
                textWriter.WriteLine(a.Key + "," + a.Value);
                return true;
            });
            textWriter.Close();
        }

        /// <summary>
        /// This function is used to read the file from disc, and populate the 
        /// list of line items in the lines collection.
        /// </summary>
        /// <param name="fileName"></param>
        private static List<LineItem> ReadFile(string fileName)
        {
            List<LineItem> lineItems = new List<LineItem>();
            //Try open the file, and read each of the lines in turn, add the lines to the collection
            try
            {
                var streamReader = new StreamReader(fileName);
                var line = "";
                //The first line in the file is the col names, so we will skip it.
                line = streamReader.ReadLine();
                while ((line = streamReader.ReadLine()) != null)
                {
                    var row = line.Split(','); //We need to read 4 fields from the file.
                    lineItems.Add(new LineItem(row[0], row[1], row[2], row[3]));
                }
            }
            catch (Exception)
            {
                    
                throw;
            }
            return lineItems;
        }
    }
}
