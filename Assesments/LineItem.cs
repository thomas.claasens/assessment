﻿using System.Dynamic;

namespace Assesments
{
    /// <summary>
    /// FirstName,LastName,Address,PhoneNumber
    /// </summary>
    public class LineItem
    {
        public string  FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public LineItem()
        {
            FirstName = "";
            LastName = "";
            Address = "";
            PhoneNumber = "";
        }

        public LineItem(string firstName, string lastName, string address, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            PhoneNumber = phoneNumber;
        }
   }
}